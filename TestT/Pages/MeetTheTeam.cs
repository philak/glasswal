﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using TestT.Base1;

namespace TestT.Pages
{
    internal class MeetTheTeam
    {

        public MeetTheTeam(IWebDriver driver)
        {
            PageFactory.InitElements(DriverRunner.driver, this);
        }


        [FindsBy(How = How.XPath, Using = "//span[contains(text(), 'Meet the Team')]")]
        public IWebElement MeetTheManagementPage { get; set; }

        [FindsBy(How = How.XPath, Using = "//img[@alt='Anne-Tiedemann.cropped']")]
        public IWebElement FirstImageDispalyed { get; set; }



        public void MeetTheTeamPageDisplay()
        {
            try
            {
              Boolean MeetTheTeam = DriverRunner.driver.PageSource.Equals("MEET THE TEAM");

            }
            catch (NoSuchElementException e)
            {

            }

        }


              public void VerifyFirstImageURLImage() { 
       
                Utility.AssertElementPresent(FirstImageDispalyed);

          
        }
    }
}