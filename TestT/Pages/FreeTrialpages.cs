﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using TestT.Base1;

namespace TestT.Pages
{
    class FreeTrialpages

    {
        public FreeTrialpages(IWebDriver driver)
        {
            PageFactory.InitElements(DriverRunner.driver, this);
        }

        string Notification = "Thank you.Your details have been sent"; 
        [FindsBy(How = How.Name, Using = "your-name")]
        public IWebElement NameField { get; set; }

        [FindsBy(How = How.Name, Using = "your-email")]
        public IWebElement EmailField { get; set; }
            
        [FindsBy(How = How.Name, Using = "your-company")]
        public IWebElement CompanyName { get; set; }

        [FindsBy(How = How.Name, Using = "your-phone-cf7it-national")]
        public IWebElement PhoneNunber { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@id='wpcf7-f5736-p5734-o1']/form/div[5]")]
        public IWebElement SendRequestNotification { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@value='Send Request']")]
        public IWebElement ClkSendRequest{ get; set; }



        


        public FreeTrialpages VerifyContactUsFields()

        { // Switching from parent window to child window   

        Utility.SwitchWindow();

        Utility.AssertElementPresent(NameField);
        Utility.AssertElementPresent(EmailField);
        Utility.AssertElementPresent(CompanyName);
        Utility.AssertElementPresent(PhoneNunber);
        return new FreeTrialpages(DriverRunner.driver);
        }

        public FreeTrialpages FillContactUsField(string name,string email,string coy,string mobile)
        {
                            
        NameField.SendKeys(name);
        EmailField.SendKeys(email);
        CompanyName.SendKeys(coy);
        PhoneNunber.SendKeys(mobile);

        return new FreeTrialpages(DriverRunner.driver);
        }

        public FreeTrialpages SendRequest()

        {
        ClkSendRequest.Click();
        return new FreeTrialpages(DriverRunner.driver);
        }

        public FreeTrialpages RequestSendNotification()

        {
            Boolean RequestNotification = DriverRunner.driver.PageSource.Equals(Notification);
            return new FreeTrialpages(DriverRunner.driver);
        }


    }
}
