﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using TechTalk.SpecFlow;
using TestT.Base1;

namespace TestT.Pages
{
    class Homepage 
    {

        public Homepage(IWebDriver driver)
        {
            PageFactory.InitElements(DriverRunner.driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//span[contains(text(), 'Meet the Team')]")]
        public IWebElement MeetTheManagement { get; set; }


        [FindsBy(How = How.XPath, Using = "//span[contains(text(), 'In the News')]")]
        public IWebElement In_TheNews { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[contains(text(), 'What')]")]
        public IWebElement IsWhat { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[contains(text(), 'Free Trial')]")]
        public IWebElement HowiTWorks { get; set; }


        public MeetTheTeam Meet_TheTeam()

        {
            Utility.MouseHoverClickJS(MeetTheManagement);
            return new MeetTheTeam(DriverRunner.driver);
        }

        public InTheNews NavigateInTheNews()

        {
            Utility.MouseHoverClickJS(In_TheNews);
            return new InTheNews(DriverRunner.driver);
        }

        public HowItWorks OpenHowItWorks()

        {
            Utility.MouseHoverClickJS(HowiTWorks);
            return new HowItWorks(DriverRunner.driver);
        }
        [AfterFeature]
        public void Close() {
            DriverRunner.driver.Close();
        }
    }
}
