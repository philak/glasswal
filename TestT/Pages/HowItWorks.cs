﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestT.Base1;

namespace TestT.Pages
{
    class HowItWorks
    {

        public HowItWorks(IWebDriver driver)
        {
            PageFactory.InitElements(DriverRunner.driver, this);
        }

  

        [FindsBy(How = How.XPath, Using = ".//*[@title='Request A Free Trial']")]
        public IWebElement FreeTrialButton { get; set; }

     

      
        public FreeTrialpages OpenFreeTrialButton()

        {
            FreeTrialButton.Click();
            return new FreeTrialpages(DriverRunner.driver);
            
        }


    
    }

    
           

        }
    
