﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using TestT.Base1;

namespace TestT.Pages
{
    internal class InTheNews
    {
        public InTheNews(IWebDriver driver)
        {
            PageFactory.InitElements(DriverRunner.driver, this);

        }
    
      
        [FindsBy(How = How.XPath, Using = "//div[@class='single-blog-title-inner text-center']/h1")]
        public IWebElement Article { get; set; }

      
        public InTheNews VerifyInTheNewsTitle(string InTheNews)
        {

            Boolean MeetTheTeam = DriverRunner.driver.PageSource.Equals(InTheNews);
            return new InTheNews(DriverRunner.driver);
        }
        

             public InTheNews VerifyCorrectArticle(string article)
        {
            Boolean MeetTheTeam = DriverRunner.driver.PageSource.Equals(article);
          
             return new InTheNews(DriverRunner.driver);
        }
    }
}