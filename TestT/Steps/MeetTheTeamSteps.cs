﻿
using TechTalk.SpecFlow;
using TestT.Base1;
using TestT.Pages;

namespace TestT.Steps
{
    [Binding]
    class MeetTheTeamSteps : BaseStep
    {
        Homepage ManagementPage;
        MeetTheTeam ManagementPAge;



      

       

        [Given(@"I have navigated to the Management Team page via Company menu")]
        public void GivenIHaveNavigatedToTheManagementTeamPageViaCompanyMenu()
        {
             ManagementPage = new Homepage(DriverRunner.driver);
             ManagementPAge = ManagementPage.Meet_TheTeam();

        }

        [Given(@"Management pages is displayed correctly")]
        public void GivenManagementPagesIsDisplayedCorrectly()
        {
            ManagementPAge.MeetTheTeamPageDisplay();


      }

        [Then(@"The URL of the first image is present\.")]
        public void ThenTheURLOfTheFirstImageIsPresent_()
        {
            ManagementPAge.VerifyFirstImageURLImage();
        }

    }
}
