﻿
using TechTalk.SpecFlow;
using TestT.Base1;
using TestT.Pages;

namespace TestT.Steps
{
    [Binding]
    class InTheNewSteps : BaseStep
    {
        Homepage homepage;
        InTheNews InTheNews;
       

        [When(@"I have navigated to the “In the News” page via News")]
        public void WhenIHaveNavigatedToTheInTheNewsPageViaNews()
        {
            homepage = new Homepage(DriverRunner.driver);
            InTheNews = homepage.NavigateInTheNews();
        }

       

        [When(@"The page synopsis is ""(.*)""")]
        public void WhenThePageSynopsisIs(string title)
        {
            InTheNews.VerifyInTheNewsTitle(title);
        }



        [Then(@"Verify the following ""(.*)""")]
        public void WhenThePageSynopshisIs(string article)
        {
            InTheNews.VerifyCorrectArticle(article);
        }
    }
}
