﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TestT.Base;
using TestT.Base1;

namespace TestT.Steps
{
    [Binding]
    class CommonSteps : BaseStep

    {
        [BeforeFeature]
       // [Given(@"I open the Glass webpage")]
        public static void GivenIOpenTheGlassWebpage()
        {
            BrowserBase Launch = new BrowserBase();
            Launch.OpenApplication();
        }


        [AfterFeature]
        public static void close()
        {
            DriverRunner.driver.Quit();
        }
    }
}
