﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TestT.Base1;
using TestT.Pages;

namespace TestT.Steps
{
    [Binding]
    class HowItWorksStep : BaseStep
    {
        Homepage homepage;
        HowItWorks FreeTrial;
        FreeTrialpages Trialpages;

        [Given(@"I have navigated to How It Works page")]
        public void ThenIHaveNavigatedToHowItWorksPage()
        {
            homepage = new Homepage(DriverRunner.driver);
            FreeTrial = homepage.OpenHowItWorks();
        }

        //  [When(@"I have navigated to the free trial via How it works")]
        // public void WhenIHaveNavigatedToTheFreeTrialViaHowItWorks()
        //  {
        //     Trialpages=FreeTrial.OpenFreeTrialButton();


        // }

     


      //  [Then(@"I see name,email,company and mobile fields")]
        //public void WhenISeeNameEmailCompanyAndMobileFields()
        //{
       //     Trialpages.VerifyContactUsFields();
        //}

       

        [Then(@"I enter details in the required fields")]
        public void ThenIEnterDetailsInTheRequiredFields()
        {
            Trialpages.FillContactUsField("Tom The","gdgdg@hdhah.com","Gjhhaja LTD","0782222");
        }

        [Then(@"I send the request to Glasswall")]
        public void ThenISendForTheRequestToGlasswall()
        {
            Trialpages.SendRequest();
        }

        [When(@"I receive a notification")]
        public void WhenIReceiveANotification()
        {
            Trialpages.RequestSendNotification();
        }
    }
}







