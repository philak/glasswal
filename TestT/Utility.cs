﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestT.Base1;

namespace TestT
{
    public class Utility
    {
        static string javaScript = "var evObj = document.createEvent('MouseEvents');" +
       "evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);" +
       "arguments[0].dispatchEvent(evObj);" + "arguments[0].click();";

        static string javaScript1 = "return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0";


        public static void MouseHoverClickJS(IWebElement element1)
        {
            IJavaScriptExecutor executor = DriverRunner.driver as IJavaScriptExecutor;

            executor.ExecuteScript(javaScript, element1);
            Thread.Sleep(5000);

        }


        public static void ImageDisplayed(IWebElement element1)
        {

            Boolean ImagePresent = (Boolean)((IJavaScriptExecutor)DriverRunner.driver).ExecuteScript(javaScript1, element1);
            if (!ImagePresent)
            {
                Console.WriteLine("Image is displayed.");
            }
            else
            {
                Console.WriteLine("Image not displayed.");
            }
        }

        public static void AssertElementPresent(IWebElement element)
        {
            if (!IsElementPresent(element))
                throw new Exception(string.Format("Element Not Present exception"));
        }

        private static bool IsElementPresent(IWebElement element)
        {
            try
            {
                bool ele = element.Displayed;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void TextBoxJS(IWebElement element1)
        {

            IJavaScriptExecutor Executor = ((IJavaScriptExecutor)DriverRunner.driver);
           
            Executor.ExecuteScript("arguments[0].click();", element1);


        }

        public static void SwitchWindow()
        {

            foreach (string Child_Window in DriverRunner.driver.WindowHandles)
            {
                DriverRunner.driver.SwitchTo().Window(Child_Window);
            }


        }
           
        }
    }



