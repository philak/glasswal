﻿Feature: "In The News" page
		As a user I want go to the “In the News” Page via the “News” menu item.
So that I see the latest news section

@positive
Scenario Outline: Check "In the News" page and article displayed correctly
    
	When I have navigated to the “In the News” page via News
	And The page synopsis is "In the News"
	Then Verify the following "<article>"
	
	Examples: 
	| article |
	|  UPDATING YOUR PROPERTY’S CYBER SECURITY SYSTEMS AND EDUCATION STAFF ARE KEY TO AVOIDING DAMAGING CYBER-ATTACKS, SAYS GREGSIM OF GLASSWALL SOLUTIONS.       |