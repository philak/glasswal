﻿Feature: Management Team

      As a user I want to navigate to the “Management Team” page via the “Company” menu item.
      So that I can see that the correct page is displayed



@positive
Scenario: Check Management page and first image URL is present
  
	Given I have navigated to the Management Team page via Company menu
	And Management pages is displayed correctly
	Then The URL of the first image is present.